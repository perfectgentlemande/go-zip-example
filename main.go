package main

import (
	"archive/zip"
	"io"
	"log"
	"os"
	"path"
)

func main() {
	const (
		archiveName = "archive.zip"
		filesFolder = "files"
	)

	archive, err := os.Create(archiveName)
	if err != nil {
		log.Fatal(err)
	}
	defer archive.Close()

	zipWriter := zip.NewWriter(archive)
	defer zipWriter.Close()

	dirEntries, err := os.ReadDir(filesFolder)
	if err != nil {
		log.Println("cannot read dir:", err)
		return
	}

	files := make([]*os.File, 0, len(dirEntries))
	for i := range dirEntries {
		if dirEntries[i].IsDir() {
			continue
		}

		file, errOpen := os.Open(path.Join(filesFolder, dirEntries[i].Name()))
		if errOpen != nil {
			log.Println("cannot open file:", errOpen)
			return
		}
		files = append(files, file)
	}
	defer func() {
		for i := range files {
			if err = files[i].Close(); err != nil {
				log.Println("cannot close file:", err)
			}
		}
	}()

	for i := range files {
		w, errWr := zipWriter.Create(files[i].Name())
		if errWr != nil {
			log.Println("cannot create zip writer:", errWr)
			return
		}
		if _, err = io.Copy(w, files[i]); err != nil {
			log.Println("cannot copy file:", err)
			return
		}
	}
}